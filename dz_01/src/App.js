import React from 'react';
import './App.css';
import guests from './res/guests.json';
import Guest from './Guest/Guest'

class App extends React.Component {

	state = {
		allInfo:false,
		search : "",
		guests 
	}

	changeHandler = ( event ) => {
		this.setState({
			search: event.target.value,
			allInfo: this.state.allInfo
		})
	}
	changeAllInfo = ( ) => {
		this.setState({
			allInfo: this.state.allInfo^1
		})
	}

	render = () => {
		const { search } = this.state;
		const { allInfo } = this.state;

		return(
			<>
				<div><h1>Список гостей</h1></div>
				<div>
					<input 
						onChange={this.changeHandler}
						placeholder="Поиск по списку"
						value={ search }
					/>
					<input 
						type="checkbox"
						name="Info"
						onChange={this.changeAllInfo}
						checked={this.state.allInfo}
					/>
					<label onClick={this.changeAllInfo}>Показать всю информацию</label>
				</div>
				{
					guests.map( ({name, company,phone,address,about }, index ) => 
				
						<Guest 
							key={ index }
							name={ name }
							company={ company }
							phone={ phone }
							address={ address }
							about={ about }
							search={search}
							allinfo={allInfo}
						/> 
					
					)
				} 
			</>
		);
	}

}


export default App;
