import React, { Component, createElement } from 'react'
import './Guest.css';

class Guest extends Component {

    state = {
        attend: false
    }

    changeAttend = () => {
        this.setState({
            attend: this.state.attend ^ true
        });
    }
    getAttendStatus = () =>{
        if (this.state.attend)  return 'Убыл'; else return 'Прибыл';
    }
    searchStr= vStr => {
        
        if(vStr.toUpperCase().indexOf(this.props.search.toUpperCase())>=0) return true; else return false;
    }
    getInfo = (b,text) => {
        
        if(b) 
            return createElement("p",
                    {className:"textField"},
                    text
                    );
            else return '';
       
    }

    render = () => {
        const { changeAttend } = this;
        const { attend } = this.state;
        const { name, company,phone,address,about,allinfo } = this.props;
        let vStr =name+''+company+''+phone+''+address;
        if(allinfo)vStr =vStr+''+about;
        if(this.searchStr(vStr))
        return(
            
            <div className="guest" style={ attend ? { backgroundColor: "gray" } : {  backgroundColor:"darkseagreen" }}>
                <div>
                <p>Гость <span className="textField">{ name } </span> работает в компании <span className="textField">{ company } </span></p>
                <p>Его контакты:</p>
                <p className="textField">{ phone } </p>
                <p className="textField">{ address }  </p>
                {this.getInfo(allinfo,about)}
                </div>
                <div className="clBtn">
                <button onClick={changeAttend} >{this.getAttendStatus()}</button> 
                </div>
            </div>
            
        );else return('');
    }

}


export default Guest;